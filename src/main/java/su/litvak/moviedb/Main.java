package su.litvak.moviedb;

import su.litvak.moviedb.gui.GenresPanel;
import su.litvak.moviedb.gui.MoviesPanel;
import su.litvak.moviedb.gui.PeoplePanel;

import javax.swing.*;

public class Main {
    public static void main(String[] args) throws Exception {
        Sessions.configure();

        JFrame f = new JFrame();
        f.setTitle("Simple Movies DB");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JTabbedPane tp = new JTabbedPane();
        tp.addTab("Movies", new MoviesPanel());
        tp.addTab("People", new PeoplePanel());
        tp.addTab("Genres", new GenresPanel());
        f.getContentPane().add(tp);
        f.setVisible(true);
        f.setBounds(0,0,640,480);
    }
}
