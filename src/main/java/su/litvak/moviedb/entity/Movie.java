package su.litvak.moviedb.entity;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "movie")
public class Movie {
    @Id @GeneratedValue
    public int id;

    public String title = "";
    public int year = 2013;
    public int time = 120;
    public double rating;
    public String story;

    @OneToMany(targetEntity = Person.class)
    @JoinTable(name = "movie_actor",
               joinColumns = {@JoinColumn(name = "movie_id")},
               inverseJoinColumns = {@JoinColumn(name = "person_id")})
    public Set<Person> actors;

    @OneToMany(targetEntity = Person.class)
    @JoinTable(name = "movie_director",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "person_id")})
    public Set<Person> directors;

    @OneToMany(targetEntity = Person.class)
    @JoinTable(name = "movie_producer",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "person_id")})
    public Set<Person> producers;

    @OneToMany(targetEntity = Person.class)
    @JoinTable(name = "movie_writer",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "person_id")})
    public Set<Person> writers;

    @OneToMany(targetEntity = Genre.class)
    @JoinTable(name = "movie_genre",
               joinColumns = {@JoinColumn(name = "movie_id")},
               inverseJoinColumns = {@JoinColumn(name = "genre_id")})
    public Set<Genre> genres;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (year != movie.year) return false;
        if (title != null ? !title.equals(movie.title) : movie.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + year;
        return result;
    }

    @Override
    public String toString() {
        return title + " (" + year + ")";
    }
}
