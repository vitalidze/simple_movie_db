package su.litvak.moviedb.gui;

import su.litvak.moviedb.entity.Genre;
import su.litvak.moviedb.entity.Movie;
import su.litvak.moviedb.entity.Person;

import javax.swing.*;

public class MovieInfoPanel extends JPanel {
    JLabel lbl = new JLabel();
    Movie movie;

    public MovieInfoPanel(Movie movie) {
        this.movie = movie;

        performLayout();
    }

    private void performLayout() {
        lbl.setHorizontalAlignment(JLabel.LEFT);
        add(lbl);

        String html = "<html><head></head><body>";

        html += "<h1>" + movie.title + " (" + movie.year + ")</h1>";
        if (movie.time > 0) {
            html += "<p>" + movie.time + " minutes";
        }
        if (movie.genres.size() > 0) {
            String genres = "";
            for (Genre genre : movie.genres) {
                genres += (genres.length() > 0 ? ", " : "") + genre;
            }
            html += (movie.time > 0 ? " - " : "") + genres;
        }
        html += "</p>";

        if (movie.rating > 0) {
            html += "<p>Rating: " + movie.rating + "</p>";
        }

        if (movie.directors.size() > 0) {
            html += "<p>";
            html += "Director" + (movie.directors.size() > 1 ? "s" : "") + ": ";
            String directors = "";
            for (Person director : movie.directors) {
                directors += (directors.length() > 0 ? ", " : "") + director;
            }
            html += directors + "</p>";
        }

        if (movie.writers.size() > 0) {
            html += "<p>";
            html += "Writer" + (movie.writers.size() > 1 ? "s" : "") + ": ";
            String writers = "";
            for (Person writer : movie.writers) {
                writers += (writers.length() > 0 ? "; " : "") + writer;
            }
            html += writers + "</p>";
        }

        if (movie.producers.size() > 0) {
            html += "<p>";
            html += "Producer" + (movie.producers.size() > 1 ? "s" : "") + ": ";
            String producers = "";
            for (Person producer : movie.producers) {
                producers += (producers.length() > 0 ? "; " : "") + producer;
            }
            html += producers + "</p>";
        }

        if (movie.actors.size() > 0) {
            html += "<p>";
            html += "Star" + (movie.actors.size() > 1 ? "s" : "") + ": ";
            String actors = "";
            for (Person actor : movie.actors) {
                actors += (actors.length() > 0 ? "; " : "") + actor;
            }
            html += actors + "</p>";
        }

        if (movie.story != null && movie.story.trim().length() > 0) {
            html += "<h2>Story</h2>";
            html += "<p>" + movie.story + "</p>";
        }

        html += "</body></html>";
        lbl.setText(html);
    }
}
