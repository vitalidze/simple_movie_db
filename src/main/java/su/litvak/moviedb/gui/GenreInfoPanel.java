package su.litvak.moviedb.gui;

import su.litvak.moviedb.Sessions;
import su.litvak.moviedb.entity.Genre;
import su.litvak.moviedb.entity.Movie;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class GenreInfoPanel extends JPanel {
    JLabel lbl = new JLabel();
    Genre genre;

    public GenreInfoPanel(Genre genre) {
        this.genre = genre;

        performLayout();
    }

    private void performLayout() {
        lbl.setHorizontalAlignment(JLabel.LEFT);
        JScrollPane sp = new JScrollPane(lbl);
        add(sp);
        sp.setPreferredSize(new Dimension(600, 800));
        sp.setMaximumSize(new Dimension(600, 800));

        String html = "<html><head></head><body>";

        html += "<h1>" + genre.title + "</h1>";

        List<Movie> movies = Sessions.getSession().createSQLQuery(
                "SELECT m.* FROM movie m" +
                " INNER JOIN movie_genre mg ON m.id=mg.movie_id" +
                " WHERE mg.genre_id=:genreId" +
                " ORDER BY m.year DESC")
                .addEntity(Movie.class)
                .setInteger("genreId", genre.id)
                .list();

        int year = -1;
        for (Movie movie : movies) {
            if (year < 0 || movie.year != year) {
                year = movie.year;
                html += "<h2>" + year + "</h2>";
            }

            html += "<p>" + movie.title + "</p>";
        }

        html += "</body></html>";
        lbl.setText(html);
    }
}
