package su.litvak.moviedb.gui;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import su.litvak.moviedb.Sessions;
import su.litvak.moviedb.entity.Movie;
import su.litvak.moviedb.entity.Person;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PeoplePanel extends JPanel {
    JTextField fldSearch = new JTextField();
    JButton btnSearch = new JButton("Search");

    Model tblModel = new Model();
    JTable tblPeople = new JTable(tblModel);
    JScrollPane spTable = new JScrollPane(tblPeople);

    JButton btnInfo = new JButton("Show info");
    JButton btnAdd = new JButton("Add");
    JButton btnEdit = new JButton("Edit");
    JButton btnDelete = new JButton("Delete");

    static class Model extends AbstractTableModel {
        List<Person> people = new ArrayList<Person>();

        @Override
        public int getRowCount() {
            return people.size();
        }

        @Override
        public int getColumnCount() {
            return 1;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Person person = people.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    return person.name;
            }

            return null;
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return " Title ";
            }
            return null;
        }
    }

    public PeoplePanel() {
        performLayout();
    }

    private void performLayout() {
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);

        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addComponent(fldSearch)
                        .addComponent(btnSearch)
                )
                .addComponent(spTable)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(btnInfo)
                        .addComponent(btnAdd)
                        .addComponent(btnEdit)
                        .addComponent(btnDelete)
                )
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(fldSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSearch)
                )
                .addComponent(spTable)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(btnInfo)
                        .addComponent(btnAdd)
                        .addComponent(btnEdit)
                        .addComponent(btnDelete)
                )
        );

        /**
         * Set up table
         */
        tblPeople.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        /**
         * Set up search function
         */
        ActionListener al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                search();
            }
        };
        btnSearch.addActionListener(al);
        fldSearch.addActionListener(al);

        /**
         * Set up delete function
         */
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                delete();
            }
        });

        /**
         * Set up add function
         */
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add();
            }
        });

        /**
         * Set up edit function
         */
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                update();
            }
        });

        btnInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tblPeople.getSelectedRow() < 0) {
                    return;
                }

                showPersonInfo();
            }
        });
    }

    private void search() {
        String s = fldSearch.getText().trim();
        if (s.isEmpty()) {
            tblModel.people.clear();
        } else {
            Sessions.beginTransaction();
            try {
                tblModel.people = Sessions.getSession().createCriteria(Person.class)
                        .add(Restrictions.like("name", "%" + s + "%"))
                        .addOrder(Order.asc("name")).list();
            } finally {
                Sessions.rollbackTransaction();
            }
        }

        tblModel.fireTableDataChanged();
    }

    private void add() {
        String s = JOptionPane.showInputDialog(PeoplePanel.this, "Enter new name for a new person",  "");
        if (s == null || s.trim().isEmpty()) {
            return;
        }
        s = s.trim();
        try {
            Sessions.beginTransaction();
            Person existing = (Person) Sessions.getSession().createCriteria(Person.class).add(Restrictions.eq("name", s)).uniqueResult();
            if (existing != null) {
                Sessions.rollbackTransaction();
                JOptionPane.showMessageDialog(this, "Person '" + existing + "' already exists.", "Add person", JOptionPane.WARNING_MESSAGE);
                return;
            }

            Person newPerson = new Person();
            newPerson.name = s;
            Sessions.getSession().save(newPerson);

            Sessions.commitTransaction();

            JOptionPane.showMessageDialog(this, "Person '" + newPerson + "' was successfully added", "Add person", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            Sessions.rollbackTransaction();
            JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Add person", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void update() {
        if (tblPeople.getSelectedRow() < 0) {
            return;
        }

        Person person = tblModel.people.get(tblPeople.getSelectedRow());
        String s = JOptionPane.showInputDialog(PeoplePanel.this, "Enter new name for '" + person + "'",  person.name);
        if (s == null || s.trim().isEmpty()) {
            return;
        }
        s = s.trim();
        try {
            Sessions.beginTransaction();
            Person existing = (Person) Sessions.getSession().createCriteria(Person.class).add(Restrictions.eq("name", s)).uniqueResult();
            if (existing != null && existing.id != person.id) {
                Sessions.rollbackTransaction();
                JOptionPane.showMessageDialog(this, "Person '" + existing + "' already exists.", "Add person", JOptionPane.WARNING_MESSAGE);
                return;
            }

            person.name = s;
            Sessions.getSession().update(person);

            Sessions.commitTransaction();

            JOptionPane.showMessageDialog(this, "Person '" + person + "' was successfully updated", "Update person", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            Sessions.rollbackTransaction();
            JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Update person", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void delete() {
        int row = tblPeople.getSelectedRow();
        if (row < 0) {
            return;
        }

        Person person = tblModel.people.get(row);
        if (JOptionPane.showConfirmDialog(this, "Are you sure you want to delete '" + person + "'?", "Delete person", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            return;
        }
        Sessions.beginTransaction();
        try {
            for (String table : new String[] {
                    "movie_director",
                    "movie_writer",
                    "movie_producer",
                    "movie_actor"
            }) {
                Sessions.getSession().createSQLQuery("DELETE FROM " + table + " WHERE person_id=:personId").setInteger("personId", person.id).executeUpdate();
            }

            Sessions.getSession().refresh(person);
            Sessions.getSession().delete(person);
            Sessions.commitTransaction();
            JOptionPane.showMessageDialog(this, "Person '" + person + "' was successfully deleted", "Delete person", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            Sessions.rollbackTransaction();
            JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Delete person", JOptionPane.ERROR_MESSAGE);
        }
    }

    private JFrame getFrame() {
        Container parent = getParent();
        while (!(parent instanceof JFrame)) {
            parent = parent.getParent();
        }
        return (JFrame) parent;
    }

    private void showPersonInfo() {
        JDialog dialog = new JDialog(getFrame(), "Person info", true);
        Sessions.beginTransaction();
        Person person = tblModel.people.get(tblPeople.getSelectedRow());
        PersonInfoPanel panel = new PersonInfoPanel((Person) Sessions.getSession().get(Person.class, person.id));
        Sessions.rollbackTransaction();

        /**
         * Show dialog
         */
        dialog.setContentPane(panel);
        dialog.pack();
        dialog.setVisible(true);
    }
}
