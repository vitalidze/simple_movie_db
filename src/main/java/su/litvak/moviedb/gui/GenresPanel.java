package su.litvak.moviedb.gui;

import org.hibernate.criterion.Order;
import su.litvak.moviedb.Sessions;
import su.litvak.moviedb.entity.Genre;
import su.litvak.moviedb.entity.Movie;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GenresPanel extends JPanel {

    static class GenreDesc implements ActionListener {
        JTextField fldGenre = new JTextField();
        JButton btnInfo = new JButton("Info");
        JButton btnDelete = new JButton("Delete");
        Genre genre;

        GenreDesc(Genre genre) {
            this.genre = genre;
            fldGenre.setText(genre.title);

            subscribe();
        }

        GenreDesc() {
            this.genre = new Genre();

            subscribe();
        }

        private void subscribe() {
            btnInfo.addActionListener(this);
            btnDelete.addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == btnInfo) {
                showInfo();
            } else if (e.getSource() == btnDelete) {
                delete();
            }
        }

        void removeComponents() {
            JComponent parent = (JComponent) fldGenre.getParent();
            parent.remove(btnInfo);
            parent.remove(btnDelete);
            parent.remove(fldGenre);

            parent.revalidate();
        }

        boolean hasChanges() {
            return genre.id <= 0 || !genre.title.equals(fldGenre.getText().trim());
        }

        void delete() {
            if (genre.id > 0) {
                if (JOptionPane.showConfirmDialog(fldGenre.getParent(), "Are you sure you want to delete '" + genre + "'?", "Delete genre", JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                    return;
                }

                try {
                    Sessions.beginTransaction();
                    Sessions.getSession().createSQLQuery("DELETE FROM movie_genre WHERE genre_id=:genreId").setInteger("genreId", genre.id).executeUpdate();
                    Sessions.getSession().delete(Sessions.getSession().get(Genre.class, genre.id));
                    Sessions.commitTransaction();
                } catch (Exception ex) {
                    Sessions.rollbackTransaction();
                    JOptionPane.showMessageDialog(fldGenre.getParent(), ex.getLocalizedMessage(), "Delete genre", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }

            removeComponents();
        }

        private JFrame getFrame() {
            Container parent = fldGenre.getParent();
            while (!(parent instanceof JFrame)) {
                parent = parent.getParent();
            }
            return (JFrame) parent;
        }

        void showInfo() {
            if (genre.id <= 0) {
                return;
            }

            JDialog dialog = new JDialog(getFrame(), "Genre info", true);
            Sessions.beginTransaction();
            GenreInfoPanel panel = new GenreInfoPanel((Genre) Sessions.getSession().get(Genre.class, genre.id));
            Sessions.rollbackTransaction();

            /**
             * Show dialog
             */
            dialog.setContentPane(panel);
            dialog.pack();
            dialog.setVisible(true);
        }
    }

    JPanel jpGenres = new JPanel();
    JScrollPane spGenres = new JScrollPane(jpGenres);
    List<GenreDesc> lstGenres = new ArrayList<GenreDesc>();
    JButton btnAdd = new JButton("Add new genre");

    JButton btnSave = new JButton("Save");
    JButton btnCancel = new JButton("Cancel");

    public GenresPanel() {
        performLayout();
    }

    private void performLayout() {
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);

        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);

        final GroupLayout layoutGenres = new GroupLayout(jpGenres);
        jpGenres.setLayout(layoutGenres);
        layoutGenres.setAutoCreateContainerGaps(true);
        layoutGenres.setAutoCreateGaps(true);

        final GroupLayout.Group genreHorGroup = layoutGenres.createParallelGroup();
        final GroupLayout.Group genreVerGroup = layoutGenres.createSequentialGroup();

        layoutGenres.setHorizontalGroup(genreHorGroup);
        layoutGenres.setVerticalGroup(genreVerGroup);

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addComponent(spGenres)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addComponent(btnSave)
                        .addComponent(btnCancel)
                )
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(spGenres)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(btnAdd)
                        .addComponent(btnSave)
                        .addComponent(btnCancel)
                )
        );

        /**
         * Load existing genres
         */
        Sessions.beginTransaction();

        for (Genre genre : (List<Genre>) Sessions.getSession().createCriteria(Genre.class).addOrder(Order.asc("title")).list()) {
            addGenre(genre, layoutGenres, genreHorGroup, genreVerGroup);
        }

        Sessions.rollbackTransaction();

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addGenre(null, layoutGenres, genreHorGroup, genreVerGroup);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        spGenres.getVerticalScrollBar().setValue(spGenres.getVerticalScrollBar().getMaximum());
                    }
                });
            }
        });

        btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveChanges();
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelChanges();
            }
        });
    }

    private void addGenre(Genre genre, GroupLayout layout, GroupLayout.Group horGroup, GroupLayout.Group verGroup) {
        GenreDesc desc = genre == null ? new GenreDesc() : new GenreDesc(genre);
        horGroup.addGroup(layout.createSequentialGroup()
                .addComponent(desc.fldGenre)
                .addComponent(desc.btnInfo)
                .addComponent(desc.btnDelete)
        );

        verGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addComponent(desc.fldGenre, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(desc.btnInfo)
                .addComponent(desc.btnDelete)
        );

        revalidate();

        lstGenres.add(desc);
    }

    private void cancelChanges() {
        for (Iterator<GenreDesc> it = lstGenres.iterator(); it.hasNext(); ) {
            GenreDesc desc = it.next();

            if (desc.genre.id > 0) {
                desc.fldGenre.setText(desc.genre.title);
            } else {
                desc.removeComponents();
                it.remove();
            }
        }
    }

    private void saveChanges() {
        try {
            Sessions.beginTransaction();

            for (GenreDesc desc : lstGenres) {
                if (desc.hasChanges()) {
                    desc.genre.title = desc.fldGenre.getText().trim();
                    Sessions.getSession().saveOrUpdate(desc.genre);
                }
            }

            Sessions.commitTransaction();
            JOptionPane.showMessageDialog(this, "Changes were saved successfully", "Save genres", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception ex) {
            Sessions.rollbackTransaction();
            JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Save genres", JOptionPane.ERROR_MESSAGE);
        }
    }
}
