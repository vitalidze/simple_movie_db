package su.litvak.moviedb.gui;

import su.litvak.moviedb.Sessions;
import su.litvak.moviedb.entity.Movie;
import su.litvak.moviedb.entity.Person;

import javax.swing.*;
import java.awt.*;

public class PersonInfoPanel extends JPanel {
    JLabel lbl = new JLabel();
    Person person;

    public PersonInfoPanel(Person person) {
        this.person = person;

        performLayout();
    }

    private void performLayout() {
        lbl.setHorizontalAlignment(JLabel.LEFT);
        JScrollPane sp = new JScrollPane(lbl);
        add(sp);

        String html = "<html><head></head><body>";

        html += "<h1>" + person + "</h1>";

        for (String[] role : new String[][] {
                {"Director", "movie_director"},
                {"Writer", "movie_writer"},
                {"Producer", "movie_producer"},
                {"Star", "movie_actor"},
        }) {
            String roleHtml = getRolesString(role[1]);
            if (roleHtml.isEmpty()) {
                continue;
            }
            html += "<h2>" + role[0] + "</h1>";
            html += roleHtml;
        }

        html += "</body></html>";
        lbl.setText(html);

        sp.setPreferredSize(new Dimension(600, Math.min(lbl.getPreferredSize().height + 50, 800)));
    }

    private String getRolesString(String tableName) {

        java.util.List<Movie> movies = Sessions.getSession().createSQLQuery(
                "SELECT m.* FROM movie m" +
                " INNER JOIN " + tableName + " mr ON m.id=mr.movie_id" +
                " WHERE mr.person_id=:personId" +
                " ORDER BY m.year DESC")
                .addEntity(Movie.class)
                .setInteger("personId", person.id)
                .list();

        String html = "";
        for (Movie movie : movies) {
            html += "<p>" + movie.title + "</p>";
        }
        return html;
    }
}
