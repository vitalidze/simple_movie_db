package su.litvak.moviedb;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import su.litvak.moviedb.entity.Genre;
import su.litvak.moviedb.entity.Movie;
import su.litvak.moviedb.entity.Person;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Sessions {
    public final static String SETTINGS_CFG = System.getProperty("user.dir") + File.separatorChar + "settings.cfg";
    public final static String PROP_JDBC_URL = "jdbc.url";
    public final static String PROP_JDBC_USER = "jdbc.user";
    public final static String PROP_JDBC_PASSWORD = "jdbc.password";

    private static SessionFactory sessionFactory;

    public static synchronized void configure() throws IOException {
        if (sessionFactory != null) {
            sessionFactory.close();
        }

        /**
         * Load properties from settings.cfg
         */
        Properties props = new Properties();
        InputStream is = null;
        try {
            is = new FileInputStream(SETTINGS_CFG);
            props.load(is);
        } finally {
            is.close();
        }

        /**
         * Configure and build session factory
         */
        sessionFactory = new Configuration()
                    .addPackage("su.litvak.moviedb.entity")
                    .addAnnotatedClass(Movie.class)
                    .addAnnotatedClass(Genre.class)
                    .addAnnotatedClass(Person.class)

                    .setProperty("hibernate.connection.url", props.getProperty(PROP_JDBC_URL))
                    .setProperty("hibernate.connection.username", props.getProperty(PROP_JDBC_USER))
                    .setProperty("hibernate.connection.password", props.getProperty(PROP_JDBC_PASSWORD))
                .configure()
                .buildSessionFactory();
    }

    public static Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public static void beginTransaction() {
        if (getSession().getTransaction() == null ||
            !getSession().getTransaction().isActive()) {
            getSession().beginTransaction();
        }
    }

    public static void commitTransaction() {
        if (getSession().getTransaction() != null &&
            getSession().getTransaction().isActive()) {
            getSession().getTransaction().commit();
        }
    }

    public static void rollbackTransaction() {
        if (getSession().getTransaction() != null &&
            getSession().getTransaction().isActive()) {
            getSession().getTransaction().rollback();
        }
    }
}
