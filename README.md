To make it working you will need:

1) Database created in part two. I am attaching the mysql dump file so you can just restore it.

2) Edit settings.cfg file and enter there correct:

* jdbc.url - a special url used to describe connection to the database. Now it's set up to connect to the database 'moviesdb' located on PC named 'server-vitaly'. You should change it according to your configuration.

* jdbc.user - name of user which have full access to the movies database

* jdbc.password - password of user with name specified in jdbc.user property

3) Place settings.cfg file into same folder as the application jar file and run it. You can do it in two ways:

* double-click in windows explorer

* in command-line tool (like cmd.exe in windows) type following command:

    java -jar simple_movie_db-0.0.1-jar-with-dependencies.jar